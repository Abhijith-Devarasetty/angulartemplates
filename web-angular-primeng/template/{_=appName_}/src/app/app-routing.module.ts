<%let components = utils.getRouting(data)-%>
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
<%components.forEach(com => {-%>
  <%com = com.componentName-%>
  import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>/<%=utils.compName(com,"dash")%>.component';
  <%})-%>

const routes: Routes = [
  <%components.forEach(com => {-%>
  <%com = com.componentName-%>
    { path: '<%=utils.compName(com,"dash")%>', component: <%=com%>Component},
  <%})-%>
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [ 
  <%components.forEach(com => {-%>
  <%com = com.componentName-%>
    <%=com%>Component,
  <%})-%>
  ];

