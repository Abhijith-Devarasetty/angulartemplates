const { processData } = require("./processdata.js");
let jsonata = require("jsonata");

module.exports.run = (inputData, lookup, tasks, utils) => {
  let temp = {
    applications: [inputData],
  };
  console.log("input data", JSON.stringify(temp));
  // tasks["Form"] = lookup.components;
  let processedData = processData(temp,utils.getRef);
  console.log("processed data", JSON.stringify(processedData));
  tasks["TabLayoutForm"] = processedData.projects.components.filter(c => c.taskId === "TabLayoutForm");
  tasks["Form"] = processedData.projects.components.filter(c => c.taskId === "Form");
  // console.log("tasks",JSON.stringify(tasks));
  return { tasks, data: processedData};
};
