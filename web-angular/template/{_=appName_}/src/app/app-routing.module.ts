<%let components = utils.getRouting(data)-%>
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
<%components.forEach(com => {-%>
<%com = com.componentName-%>
// import { <%=com%>EditComponent } from './<%=utils.compName(com,"dash")%>-edit/<%=utils.compName(com,"dash")%>-edit.component';
import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>/<%=utils.compName(com,"dash")%>.component';
<%})-%>

const routes: Routes = [
<%components.forEach(com => {-%>
<%com = com.componentName-%>
  // { path: '<%=utils.compName(com,"dash")%>-edit', component: <%=com%>EditComponent},
  { path: '<%=utils.compName(com,"dash")%>', component: <%=com%>Component},
<%})-%>
  { path: '', component: MainComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [ 
<%components.forEach(com => {-%>
<%com = com.componentName-%>
  // <%=com%>EditComponent,
  <%=com%>Component,
<%})-%>
  MainComponent
];
